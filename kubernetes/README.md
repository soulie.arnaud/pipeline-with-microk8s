# Kubernetes


1- Global Deployment Kubernetes Objects
---------------------------------------

deployment-staging.yaml file contains all resources to set up and deploy Click Count application and Redis master and slave 
databases on kubernetes cluster for staging environment.

deployment-production.yaml file contains all resources to set up and deploy Click Count application and Redis master and slave 
databases on kubernetes cluster for production environment.

- There are three deployment objects:

frontend creates one (or more) pod(s) that contains each one click Count application. 
redis-master creates one pod that contains  redis-master application. 
redis-slave creates one (or more ) pod(s) that contains each one redis-slave application. 

- There are three service objects:

frontend-service with Nodeport (or Loadbalancer) service.
redis-master-service with Nodeport service.
redis-slave-service with Nodeport service.

2 - Frontend ClickCount Manifest
--------------------------------

frontend-deployment.yaml file allows from Docker Hub to download and run an image containing click count application.
frontend-service.yaml file allows to access and broadcast Click Count application from Kubernetes Node.

3 - Backend Redis Master and slave Manifest
------------------------------------------

redis-master-deployment.yaml allows to stock data. 
redis-master-service.yaml allows accessibility to read, write from redis master database.
redis-slave-deployment.yaml allows to read stocked data from master if this one is unavailable.
redis-slave-service.yaml allows accessibility to read only from redis slave database.


Author information
------------------

- Author: Patrick LOGA
- GitLab account: gitlab.com/ploga