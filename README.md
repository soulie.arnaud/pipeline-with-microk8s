# Click Count application

[![Build Status](https://travis-ci.org/xebia-france/click-count.svg)](https://travis-ci.org/xebia-france/click-count)


# pipeline-with-microk8s 


Objectif
--------

- Mise en place d'une plateforme d'integration continue, permettant via un pipeline, de livrer en continue les évolutions 
de l'application Click Count, et de sa base de données Redis. 

ou

- Comment déployer sur un serveur web local, l'application Click Count à l'aide d'un pipeline CI/CD.

Prérequis
---------

- Un dépôt GitLab pour stocker les codes sources nécessaire à la réalisation de la plateforme et de l'application.
- Un compte sur Docker Hub (hub.docker.com) pour stocker les images qui seront ensuite utilisées dans 
les conteneurs de Kubernetes pour le déploiement de l'applicaton.


Fichiers
--------
1. Résumé
- L'application Click Count a pour source le fichier pom.xml et le dossier src.  
- La plateforme d'integration continue a pour source les fichiers Jenkinsfile, et, 
Dockerfile, ainsi que les dossiers ansible et kubernetes. 

2. README des rôles Ansible:

- [README de Openjdk8](ansible/roles/openjdk8/README.md)
- [README de Jenkins](ansible/roles/jenkins/README.md)
- [README de Docker](ansible/roles/docker/README.md)
- [README de MicroK8s](ansible/roles/microk8s/README.md)

3. README pour Kubernetes:
- [README de Kubernetes](kubernetes/README.md)

Description de l'infrastructure
-------------------------------


La plateforme d'integration continue sera construite sur la machine locale de l'utilisateur.


Au préalable, à la ligne 4 de [Jenkinsfile](Jenkinsfile), on remplacera "yourDockerLogin" par le login docker où est stocké l'image de l'application Click Count à déployer. Ainsi qu' à la ligne 23 de [Jenkinsfile](Jenkinsfile), on remplacera "yourGitLabLogin" par le nom du compte gitlab sur lequel le dépôt pipeline-with-microk8s est localisé. On remplacera également à la ligne 19 des fichiers [deployment-staging.yaml](kubernetes/deployment-staging.yaml) et [deployment-production.yaml](kubernetes/deployment-production.yaml), "yourDockerLogin" par le login Docker où est stocké l'image de l'application Click Count à déployer.


1. Mise en place des machines virtuels (vms) 

Dans le repertoire ansible, écrire dans le shell:  

```bash
user@ubuntu:~/pipeline-with-microk8s$ cd ansible
user@ubuntu:~/pipeline-with-microk8s/ansible$ vagrant up 
```

Suite à cette action, trois vms vont se créer: 
- jenkins-master pour orchestrer l'integration continue.
- jenkins-node pour créer l'image.
- kubernetes-node pour le déploiement continue de l'application Click Count.

Puis, connecter sa machine locale en créant une paire de clé ssh, 
afin de transmettre la clé publique ssh aux trois vms. 

2. Installation et configuration des applications 

Dans le repertoire ansible, écrire dans le shell: 

```bash
user@ubuntu:~/pipeline-with-microk8s/ansible$ ansible-playbook -i inventories/dev/hosts site.yml
```

Les playbooks ansible vont installés et configurés les applications nécessaires 
à la plateforme d'integration continue.
- Le serveur jenkins-master hébergera les applications Openjdk8 et Jenkins.
- Le serveur jenkins-node hébergera les applications Openjdk8 et Docker.
- Le serveur kubernetes-node hébergera les applications Openjdk8 et MicroK8s 
qui est un cluster à un seul node Kubernetes.

Ensuite, il faudra configuré les serveurs pour que le serveur jenkins-master 
soit le serveur maître afin que jenkins-node et kubernetes-nodes soient ses serveurs esclaves.  

3. Dans Kubernetes-node, voici les étapes pour créer un token.

```bash
# On passe sous jenkins
vagrant@kubernetes-node:~$ su - jenkins

# On crée un token 
jenkins@kubernetes-node:~$token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)

# On affiche les informations sur le secret généré  
jenkins@kubernetes-node:~$ microk8s kubectl -n kube-system describe secret $token
Name:         default-token-jplkb
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: default
              kubernetes.io/service-account.uid: b5ca56d0-2d0e-4678-90ce-c9e8d12201e3

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1103 bytes
namespace:  11 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6IjNYSElKeUo0RXFEdGRwYlZsTHI0SUY2OURMdE1sTWNmbEQ2b19Lb2U5MHcifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLWpwbGtiIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJiNWNhNTZkMC0yZDBlLTQ2NzgtOTBjZS1jOWU4ZDEyMjAxZTMiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06ZGVmYXVsdCJ9.VGyy8_VLKI3I5-vEe7VAXISWkxD-Yf2dDZuBW3try9XLBzk9mAWW9dvh2h7DbYdjXdX8-teyVoDLH1XyinDpePuXVRa7MOoviYpd4MnKjRonvZ63Eb1fzBlbr1aUnurKfCfg2fB2L_T-6S5oQui1siDXPhrrsxHldQ0FscGD6FxXLNx5TqtgwW3G3Djh_JUJlO-st1Z-LgvZq_HmNXLOMf1wQDZeoHLK9Y4MNYtaaadUaQtvYXNt0PAt5YIpgnw3kRNnjtETPYs3OhTG8y2_hGax-vD7QJjxuQ6XgDGBC6UAwwEhFGsBm4JIiTLp2W8m2oMmB2H-eMPKO_3IlmyF2Q

```

Ainsi, on récupère le contenu du token: 

- Le nom du token est default-token-jplkb
- Le contenu à copier-coller dans un fichier texte est le suivant, juste après le champ token :

```
eyJhbGciOiJSUzI1NiI ..................

```

Ensuite, dans l'interface graphique de Jenkins, il faut aller sur:
``` 
Credentials -> global -> add credentials
Dans add credentials
Kind: Secret file
File:  selectionner le fichier contenant le token
ID: k8s-id
```
Cliquez sur OK pour terminer.

4. Pour finir, toujours dans l'interface graphique de Jenkins on crée un job pour exécuter le pipeline.


Dans Kubernetes, chaque environnement staging et production contiendra l'application Click Count conteneurisée, 
et, chaque application sera liée à une base de données Redis-master qui sera elle-même liée
à une base de données Redis-slave.   
Il y a une possiblité de monitoring avec Grafana déjà préinstallé dans MicroK8s. On pourra suivre la consommation cpu, 
de mémoire vive, et, le traffic réseau des pods créés, de l'application Click Count et de sa base de données dédiée Redis
sur chacun de ses environnements staging et de production.

Description du pipeline
-----------------------

Le pipeline va récupéré tous les fichiers sources du repo pipeline-with-microk8s de Gitlab et en exécuter le Jenkinsfile. L'application Click Count sera crée, à partir des sources pom.xml et du répertoire src, grâce à deux applications cloud qui sont jdk1.8.0 et maven.
A l'aide de Docker, ClickCount.war sera stocké et déployé dans un conteneur web nommé tomcat, puis une image sera créée sur le serveur jenkins-node (node01 dans le Jenkinsfile).
Une fois l'image clickimage créée, elle sera envoyée et stockée dans le registry en ligne de Docker appelé Docker Hub (hub.docker.com).
Depuis Docker Hub, l'application click Count sera téléchargée puis deployée dans le serveur kubernetes-node (node02 dans le jenkinsfile).

Pour se connecter, à l'interface graphique utilisateur Click Count, il faudra ouvrir un navigateur web, et écrire:

pour accéder à l'interface de l'environnement staging

```

192.168.60.14:30001/clickCount

```
pour accéder à l'interface de l'environnement production

```

192.168.60.14:30002/clickCount

```


Schéma du pipeline
---------------------

![Screenshot](.image/schema_pipeline.png)


Environnement
-------------

- Visual studio code 1.44.0 
- VirtualBox 5.2.34
- Git 2.17.1
- Vagrant 2.0.2
- Ansible 2.5.1
- Openjdk8
- JDK 1.8.0
- Maven 3.6.3
- Docker
- Tomcat 9.0 
- Ubuntu 18.04 
- Microk8s 1.18/stable
- Grafana 

Configuration 
-------------

Recommandée:

- Processeur: Intel Core i7
- Mémoire vive: 32 Go


Licence
-------

BSD

Information sur l'auteur
-------------------------

- Auteur: Patrick LOGA
- Compte GitLab : gitlab.com/ploga
- Compte Linkedin: linkedin.com/in/patrick-loga