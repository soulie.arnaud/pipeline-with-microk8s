pipeline {

    environment {
        registry = "yourDockerLogin/clickimage"
        registryCredential = 'dockerhub'
        dockerImage = ''
    }

    agent {   
    
        node { label 'node01' }
    }

    tools {
        jdk 'jdk1.8.0'
        maven 'maven'       
    }

    stages {

        stage('Checkout') {
            steps {
                git branch: 'master', credentialsId: 'ssh-jenkins-user', url: 'https://gitlab.com/yourGitLabLogin/pipeline-with-microk8s.git'          
            }
        }
        
        stage ('Build Maven Project') {
            
            steps {
                sh 'mvn clean package'
                }
            }

        stage('Building image') {
            steps{
                script {
                        dockerImage = docker.build registry + ":$BUILD_NUMBER"
                }
            }
        }

        stage('Upload Image') {
            steps{
                script {
                        docker.withRegistry( '', registryCredential ) {
                            dockerImage.push()
                        }
                }
            }
        }

        stage('Remove Unused docker image') {
            steps{
                sh "docker rmi $registry:$BUILD_NUMBER"
            }
        }

        stage('Deploy to Microk8s staging environment') {
           agent {   node { label 'node02' }
           }



            steps{
                
                sh "sed -i 's/clickimage:latest/clickimage:${env.BUILD_ID}/g' kubernetes/deployment-staging.yaml"

                 withCredentials([file(credentialsId: 'k8s-id', variable: 'k8stoken')]) {
                    sh 'microk8s.kubectl apply -f kubernetes/deployment-staging.yaml -n staging'
                 }
            }
        }


        stage('Deploy to Microk8s production environment') {
           agent {   node { label 'node02' }
           }

            steps{
                sh "sed -i 's/clickimage:latest/clickimage:${env.BUILD_ID}/g' kubernetes/deployment-production.yaml"

                withCredentials([file(credentialsId: 'k8s-id', variable: 'k8stoken')]) {
                        sh 'microk8s.kubectl apply -f kubernetes/deployment-production.yaml -n production'
                }
            }
        }

    }
    
}
