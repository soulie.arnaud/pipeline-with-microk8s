

# Dockerfile 

# Get Tomcat image (Alpine version) 
FROM tomcat:9.0-alpine
# Defines variables environement CATALINA_HOME and PATH
ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH
RUN mkdir -p "$CATALINA_HOME"
# Copy clickCount.war in container 
COPY target/clickCount.war /usr/local/tomcat/webapps/
EXPOSE 8080
# Launch tomcat
CMD ["catalina.sh", "run"]



