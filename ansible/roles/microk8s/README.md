Role Name
=========

Microk8s installation.

Requirements
------------

Running on Ubuntu 18.04.

Role Variables
--------------

In defaults/main.yml:

```yaml
microk8s_stable_version: 1.18/stable

microk8s_home: /snap/bin  # This is home directory for microk8s.
microk8s_var_file: environment # microk8s configuration file.
microk8s_home_file_path: /etc # Configuration directory to put microk8s configuration file


# Microk8s rights and ownership
microk8s_owner: root
microk8s_group: root
microk8s_mode: '0440'

```

Dependencies
------------

Microk8s configuration file: templates/environment

Example Playbook
----------------

```yaml
    - hosts: servers
      roles:
         - microk8s
```

License
-------

BSD

Author Information
------------------

- Author: Patrick LOGA
- GitLab account: gitlab.com/ploga
