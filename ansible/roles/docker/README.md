Role Name
=========

Docker installation.

Requirements
------------

Running on Ubuntu 18.04.

Role Variables
--------------

in defaults/main.yml:

```yaml
# Install a list of package 
packages:
    - apt-transport-https
    - ca-certificates
    - curl
    - software-properties-common

docker_repo_key: https://download.docker.com/linux/ubuntu/gpg # Docker repository key
docker_repo_name: https://download.docker.com/linux/ubuntu bionic stable # Docker repository name
```
Dependencies
------------

No dependencies.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - docker
```
License
-------

BSD

Author Information
------------------

- Author: Patrick LOGA
- GitLab account: gitlab.com/ploga
