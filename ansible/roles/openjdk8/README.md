Role Name
=========

Openjdk8 installation.   


Requirements
------------

Running on Ubuntu 18.04.


Role Variables
--------------

In defaults/main.yml:

```yaml

jdk8_home: /usr/lib/jvm/java-8-openjdk-amd64/jre  # This is home directory for jdk8.
jdk8_var_file: environment # Java configuration file.
jdk8_home_file_path: /etc # Configuration directory to put Java configuration file

# Java rights and ownership
jdk8_owner: root 
jdk8_group: root
jdk8_mode: '0440'
```

Dependencies
------------
Java configuration file: templates/environment

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - openjdk8
```

License
-------

BSD

Author Information
------------------

- Author: Patrick LOGA
- GitLab account: gitlab.com/ploga
