Role Name
=========

Jenkins installation.  

Requirements
------------

Running on Ubuntu 18.04
You must install openjdk8 before starting Jenkins installation.

Role Variables
--------------

In defaults/main.yml:

```yaml
jenkins_id: 9B7D32F2D50582E6 # Jenkins id to get Jenkins key
jenkins_key: https://pkg.jenkins.io/debian/jenkins.io.key # Jenkins key
jenkins_repo_url: http://pkg.jenkins.io/debian-stable binary/ # Jenkins remote repository
jenkins_repo_dest: jenkins.list  # Jenkins  filename name

```
Dependencies
------------

No dependencies.

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
    - jdk8 
    - jenkins
```
License
-------

BSD

Author Information
------------------

- Author: Patrick LOGA
- GitLab account: gitlab.com/ploga
